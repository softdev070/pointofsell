/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wimonsiri.midterm;

/**
 *
 * @author Acer
 */
public class Product {

   
    private int id;
    private String Name;
    private String Brand;
    private double Price;
    private int Amount;
    
    public Product(int id, String Name, String Brand, double Price, int Amount) {
        this.id = id;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = Amount;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    
     @Override
    public String toString() {
        return "product : " + "id = " + id +"        "+ "  Name = " + Name +"        "+ " Brand = " + Brand +"        "+ " Price = " + Price +"        "+ " Amount = " + Amount ;
    }
}
