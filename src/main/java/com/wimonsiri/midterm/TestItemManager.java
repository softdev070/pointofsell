/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wimonsiri.midterm;

/**
 *
 * @author Acer
 */
public class TestItemManager {

    public static void main(String[] args) {
        System.out.println(ItemManager.getProduct());
        ItemManager.addProduct(new Product(4, "cake", "easy", 35.0, 1));
        System.out.println(ItemManager.getProduct());
        Product updateProduct = new Product(4, "pizza", "easyyy", 100.0, 2);
        ItemManager.updateProduct(3, updateProduct);
        System.out.println(ItemManager.getProduct());
        ItemManager.delProduct(updateProduct);
        System.out.println(ItemManager.getProduct());
        ItemManager.delProduct(2);
        System.out.println(ItemManager.getProduct());
    }
}
