/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wimonsiri.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class ItemManager {

    private static ArrayList<Product> ItemList = null;

    static {
        load();
        ItemList = new ArrayList<>();
        // Mock Data 

    }

    //C
    public static boolean addProduct(Product product) {
        ItemList.add(product);
        save();
        return true;
    }

    //D
    public static boolean delProduct(Product product) {
        ItemList.remove(product);
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        ItemList.remove(index);
        save();
        return true;
    }

    //R
    public static ArrayList<Product> getProduct() {
        return ItemList;
    }

    public static Product getProduct(int index) {
        return ItemList.get(index);
    }

    //U
    public static boolean updateProduct(int index, Product product) {
        ItemList.set(index, product);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("wi.dat");
            fos = new FileOutputStream(file);
            try {
                oos = new ObjectOutputStream(fos);
            } catch (IOException ex) {
                Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            oos.writeObject(ItemList);
            oos.close();
            fos.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Wi.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ItemList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ItemManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
